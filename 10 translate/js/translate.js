var menuItem = {
    id: 'translate',
    title: '使用谷歌翻译',
    contexts: ['selection']
}

// 创建菜单
chrome.contextMenus.create(menuItem)

chrome.contextMenus.onClicked.addListener(function(clickData){
    // 菜单id
    // clickData.menuItemId
    // 选择的内容
    // clickData.selectionText

    if(clickData.menuItemId == 'translate' && clickData.selectionText){
        //http://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i=你好
        var createData = {
            url: "http://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i="+clickData.selectionText,
            type: "popup",
            top: 5,
            left: 5,
            width: screen.availWidth/2,
            height: screen.availHeight/2
        }
        chrome.windows.create(createData)
    }

})