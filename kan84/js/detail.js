$(function() {
    var href = window.location.toString();

    //判断网址是否显示
    var pattern = RegExp(`htt.+:\/\/.+84.net\/bdhd\/*.+\.html`);
    if (pattern.test(href) == false) {
        return
    }

    $('body').append(`
    <div id="plug-tool-fixed">
        <div class="title"> 网盘下载 </div>
        <div class="main">
        </div>
    </div>
    `);

    // 获取网盘列表
    var panList = $('.jq-list strong>a');
    var tempPanArr = [];
    // 遍历得到列表
    panList.each(function(){
        var title = $(this).text().replace(/\s*/g,"");
        var href = $(this).attr('href');

        if( title.indexOf('网盘') >=0 ){
            tempPanArr.push({
                title: title,
                href: href,
                code: href,
                status: false
            });

        }else if( title.indexOf('雷盘') >=0 ){
            // console.log(title,'111', href)

            var pattern = RegExp(`：([a-z]|[0-9])+`);

            if(pattern.test(title)){
                var code = pattern.exec(title)[0];
                // 去除验证码
                title = title.replace(code, ' ')
                // 去除特殊符号
                code = code.replace(/\s*/g,"").replace('：', '');

                tempPanArr.push({
                    title: title,
                    href: href,
                    code: code,
                    status: true
                });
            }


        }

    });

    // console.log(tempPanArr)

    // 内容框
    var mainBox = $("#plug-tool-fixed .main");

    //判断是否有网盘
    if(tempPanArr.length <= 0){
        $("#plug-tool-fixed").remove();
    }

    tempPanArr.forEach((v)=>{
        // console.log(v)
        mainBox.append(`
        <div class="info-line">
            <a href="${v.href}" target="_blank">
                <span style="color: rgb(255, 0, 0);">${v.title}</span>
            </a>
            <input class="copyBtn" type="submit" data-code="${v.code}" value="复制">
        </div>
        `);
    })

    // 复制到剪切板
    $('#plug-tool-fixed .main .info-line .copyBtn').click(function(){

        var code = $(this).attr('data-code');

        try {
            navigator.clipboard.writeText( code );
            console.log('Page URL copied to clipboard');

            $('body').append(`
                <div id="plug-layer-fixed">
                    <div class="title"> 复制成功 </div>
                </div>
            `);

            $('#plug-layer-fixed').show();
            setTimeout(function (){
                $('#plug-layer-fixed').remove();
            },2500)

        } catch (err) {
            console.error('Failed to copy: ', err);
        }

    })





})