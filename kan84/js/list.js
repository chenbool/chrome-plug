$(function(){
    var href = window.location.toString();

    //判断网址是否显示
    var pattern = RegExp(`htt.+:\/\/.+84.net\/vod\/*.+\.html`);
    if(pattern.test(href) == false){
        return
    }

    $('body').append(`
    <div id="plug-tool-fixed" class="animate__animated bounce">
      <div class="title"> 搜索设置 </div>
      <div class="main">
          <div class="line">
            <select class="select">
              <option value="全部">全部</option>
            </select> 
          </div>
          <input class="subBtn" type="submit" value="检索">
          <input class="downBtn" type="button" value="导出">
      </div>
      <div class="foot"> 
        <span class="page">第1页/共5页</span>
        <span class="area"></span>
      </div>
    </div>
    `);

    // 插入页码
    var page = $('.short-page label').text();
    page = page.replace('当前', '').replace('/', ' 丨 ').replace('总共', '共');
    $('#plug-tool-fixed .foot .page').text(page);


    // 保存地区列表
    var areaList = [];
    // 遍历查找地址
    var contList = $('#contents li');
    contList.each(function(){
        var area = $(this).children('p.type').text().replace(/\s*/g,"").replace('地区：','');

        // if( area != '云盘专辑'){
            areaList.push(area)
        // }
    });

    // 去重复
    var areaCount = _.uniq(areaList)
    $('#plug-tool-fixed .foot .area').append('共'+areaCount.length+'地区');

    //判断是否有 地区列表
    if(areaCount.length <= 1){
        $("#plug-tool-fixed").remove();
    }

    // 插入 地区列表
    areaCount.forEach(v => {
        var count = in_array_count(v, areaList);
        $('#plug-tool-fixed .main .select').append(`
        <option value="${v} ">${v} - ${count}</option>
        `)
    });

    // 从浏览器获取存储
    chrome.storage.sync.get('kan84_area',function(res){
        if(res.kan84_area){
            // alert(res.kan84_area)
            $('#plug-tool-fixed .main .select').val(res.kan84_area)
            $('#plug-tool-fixed .subBtn').click()
        }
    })


    // 点击 检索
    $('#plug-tool-fixed .subBtn').click(function(){
        var area = $('#plug-tool-fixed .main .select').val();

        $(contList).removeClass('plug-serach');

        // 保存到 浏览器
        chrome.storage.sync.set({
            'kan84_area': area
        })

        if(area == '全部'){
            $(contList).show();
            return
        }else{
            $(contList).hide();
        }

        // 获取数组下标
        var indexArr = get_array_index(area, areaList);

        // 添加 class 显示
        indexArr.forEach(v => {
            v = parseInt(v);
            // console.log( v, '=>',contList[v] );
            $(contList[v]).show()
            $(contList[v]).addClass('plug-serach');
        });

    })

    // 点击导出
    $('#plug-tool-fixed .main .downBtn').click(function(){

        var csvList = [];
        // 遍历查找地址
        var contList = $('#contents li');

        // 下拉框
        var area = $('#plug-tool-fixed .main .select').val();
        if(area != '全部'){
            contList = $('#contents li.plug-serach')
        }

        contList.each(function(){
            var area = $(this).children('p.type').text().replace(/\s*/g,"").replace('地区：','');
            var title = $(this).children('.play-pic').attr('title');
            var href = $(this).children('.play-pic').attr('href');

            if( area != '云盘专辑'){
                areaList.push(area)

                // csv 列表
                csvList.push([title,area, href]);
            }
        });

        export_csv(csvList);
    });

    // 判断出现几次
    function in_array_count(str, areaList){
        var i = 0;
        areaList.forEach(v => {
            if(str.indexOf(v) >=0){
                i+=1;
            }
        });
        return i;
    }


    // 返回字符串出现的下标
    function get_array_index(str, areaList){
        var indexArr = [];
        areaList.forEach((v,k)=> {
            if( str.indexOf(v) >=0 ){
                indexArr.push(k);
            }
        });
        return indexArr;
    }


    // 导出csv
    function export_csv(csvData){
        csvData.unshift( ['名称', '地区','网址'] );
        const excelArray = csvData;
        const excelData = excelArray.map(data => data.join(',')).join('\r\n')

        const CsvString = 'data:application/vnd.ms-excel;charset=utf-8,\uFEFF' + encodeURIComponent(excelData)
        var x = document.createElement('A')
        x.setAttribute('href', CsvString)
        x.setAttribute('download', 'data.csv')
        document.body.appendChild(x)
        x.click()
        document.body.removeChild(x)
    }


})