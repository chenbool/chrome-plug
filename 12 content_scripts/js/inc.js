var menuItem = {
    id: 'inc',
    title: '增大字体',
    contexts: ['all']
}

// 创建菜单
chrome.contextMenus.create(menuItem)

chrome.contextMenus.onClicked.addListener(function(clickData){
    // 菜单id
    // clickData.menuItemId
    // 选择的内容
    // clickData.selectionText

    if(clickData.menuItemId == 'inc' && clickData.selectionText){
        chrome.tabs.executeScript(null ,{
            code: "var old = window.getComputedStyle(document.body).fontSize;\n" +
                "var index = old.indexOf('p');\n" +
                "var size = parseInt(old.substring(0, index));\n" +
                "var newize = size+10+'px';\n" +
                "document.body.style.fontSize = newize;"
        })
    }

})

