var menuItem = {
    id: 'tts',
    title: '朗读',
    contexts: ['selection']
}

// 创建菜单
chrome.contextMenus.create(menuItem)

chrome.contextMenus.onClicked.addListener(function(clickData){
    // 菜单id
    // clickData.menuItemId
    // 选择的内容
    // clickData.selectionText

    if(clickData.menuItemId == 'translate' && clickData.selectionText){
        chrome.tts.speak(clickData.selectionText, {
            "rate": 0.7
        })
    }

})