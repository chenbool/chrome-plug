$(function(){

    chrome.storage.sync.get('limit',function(res){
        $('#limit').val(res.limit)
    })

    $('#setLimit').click(function(){
        var limit = $('#limit').val();
        if(limit){
            chrome.storage.sync.set({'limit': limit}, function(){
                close()
            })
        }
    });

    $('#resetTotal').click(function(){
        chrome.storage.sync.set({'total': 0}, function(){
            var notifyOptions = {
                // basic image simple list
                type: 'basic',
                title: '提示',
                iconUrl: '../img/icon_128.png',
                message: '总金额清零'
            }

            chrome.notifications.create('notiyLimit', notifyOptions)
        })
    });

})

// $(function(){
    
// })