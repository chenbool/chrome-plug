$(function(){

    chrome.storage.sync.get(['total','limit'],function(res){
        $('#total').text(res.total);
        $('#limit').text(res.limit);
    });

    // http://www.kkh86.com/it/chrome-extension-doc/extensions/storage.html

    $('#add').click(function(){


        // 1 从浏览器获取存储的金额
        chrome.storage.sync.get(['total','limit'],function(res){
            var totalAmount = 0;
            if(res.total){
                totalAmount = parseFloat(res.total);
            }

            // 2 添加 金额 并储存
            var amount = $('#amount').val();
            if(amount){
                totalAmount += parseFloat(amount);

                chrome.storage.sync.set({'total': totalAmount}, function(){
                    if(totalAmount > parseFloat(res.limit)){

                        var notifyOptions = {
                            // basic image simple list
                            type: 'basic',
                            title: '金额超出限制',
                            iconUrl: 'img/icon_128.png',
                            message: '超出限制'
                        }

                        chrome.notifications.create('notiyLimit', notifyOptions)
                    }
                })
            }

            // 3 更新ui
            $('#total').text(totalAmount);
            $('#amount').val('');

        })
        

    })


})

